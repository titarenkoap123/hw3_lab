const express = require('express');
const morgan = require('morgan');
const app = express();

const mongoose = require('mongoose');
mongoose.connect('mongodb+srv://Titarenko:mongoPSW@cluster0.5m91kxr.mongodb.net/deliveryapp?retryWrites=true&w=majority');

const {authMiddleware} = require('./src/middleware/authMiddleware.js');
const {authRouter} = require('./src/routers/authRouter.js');
const {usersRouter} = require('./src/routers/usersRouter.js');
const {loadsRouter} = require('./src/routers/loadsRouter.js');
const {trucksRouter} = require('./src/routers/trucksRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/loads', authMiddleware, loadsRouter);
app.use('/api/trucks', authMiddleware, trucksRouter);

app.listen(8080);

app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({message: 'Server error'});
}