const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId
  },
  assigned_to: {
    type: String,
    default: null
  },
  type: {
    type: String
  },
  status: {
    type: String,
    default: 'IS'
  },
  dimensions: {
    width: {type: Number},
    length: {type: Number},
    height: {type: Number},
    payload: {type: Number}
  },
  created_date: {
    type: Date,
    default: Date.now()
  }
})

module.exports = {
  Truck
}