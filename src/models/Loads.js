const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId
  },
  assigned_to: {
    type: String,
    default: null
  },
  status: {
    type: String,
    default: 'NEW'
  },
  state: {
    type: String,
    default: null
  },
  name: {
    type: String,
    required: true
  },
  payload: {
    type: Number,
    required: true
  },
  pickup_address: {
    type: String,
    required: true
  },
  delivery_address: {
    type: String,
    required: true
  },
  dimensions: {
    width: {type: Number},
    length: {type: Number},
    height: {type: Number}
  },
  created_date: {
    type: Date,
    default: Date.now()
  },
  logs: [{
    message: {type: String},
    time: {type: Date}
  }]
})

module.exports = {
  Load
}