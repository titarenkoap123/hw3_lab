const {saveTruck, findTrucks, findTruck, findAndAssignTruck, findAndUpdateTruck, findUserAndDeleteTruck} = require('../services/trucksService.js');
const { findUserAndDelete } = require('../services/usersService.js');

const createTruck = async (req, res, next) => {
  console.log(req.body.type);
  const userId = req.user.userId;
  const role = req.user.role;
  const type = req.body.type;
  if (role === 'DRIVER') {
    await saveTruck({userId, type});
    res.status(200).json({"message": "Truck created successfully"});
  } else {
    res.status(400).json({"message": "You cannot create trucks. Please login as a driver"})
  }
}

const getTrucks = async (req, res, next) => {
  const driverId = req.user.userId;
  const role = req.user.role;
  if(role === 'DRIVER') {
    const trucksArray = await findTrucks(driverId);
    res.status(200).json({"trucks": trucksArray})
  } else {
    res.status(400).json({"message": "The list of trucks is available only for authorized user with driver role"})
  }
}

const getTruck = async (req, res, next) => {
  const role = req.user.role;
  const truckId = req.params.id;

  if(role === 'DRIVER') {
    const truck = await findTruck(truckId);
    if(truck) {
      res.status(200).json({"truck": truck})
    } else {
      res.status(400).json({"message": `Truck with id ${truckId} not found`})
    }
  } else {
    res.status(400).json({"message": "The list of trucks is available only for authorized user with driver role"})
  }
}

const assignTruck = async (req, res, next) => {
  const truckId = req.params.id;
  const role = req.user.role;
  const driverId = req.user.userId;
  if (role === 'DRIVER') {
    const assignTruck = await findAndAssignTruck({truckId, driverId});
    if(assignTruck) {
      res.status(200).json({"message": "Truck assigned successfully"})
    } else {
      res.status(400).json({"message": `Truck with id ${truckId} not found or you already have the assigned truck`})
    }
  } else {
    res.status(400).json({"message": "To assign the truck please authorize with driver role"})
  }
}

const updateTruck = async (req, res, next) => {
  const truckId = req.params.id;
  const update = req.body.type;
  const role = req.user.role;
  if(role === 'DRIVER') {
    const result = await findAndUpdateTruck({truckId, update});
    if (result) {
      res.status(200).json({"message": "Truck details changed successfully"})
    } else {
      res.status(400).json({"message": `You cannot update the truck with id ${truckId}`})
    }
  } else {
    res.status(400).json({"message": `You cannot update the truck. Please login as a driver`})
  }
}

const deleteTruck = async (req, res, next) => {
  const truckId = req.params.id;
  const role = req.user.role;
  if(role === 'DRIVER') {
    const result = await findUserAndDeleteTruck(truckId);
    if(result) {
      res.status(200).json({"message": "Truck deleted successfully"})
    } else {
      res.status(400).json({"message": `You cannot delete the truck with id ${truckId}`})
    }
  } else {
    res.status(400).json({"message": `You cannot delete the truck. Please login as a driver`})
  }
}

module.exports = {
  createTruck,
  getTrucks,
  getTruck,
  assignTruck,
  updateTruck,
  deleteTruck
}