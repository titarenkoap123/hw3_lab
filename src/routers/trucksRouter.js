const express = require('express');
const router = express.Router();
const {createTruck, getTrucks, getTruck, assignTruck, updateTruck, deleteTruck} = require('../controllers/trucksController.js');

router.post('/', createTruck);
router.get('/', getTrucks);
router.get('/:id', getTruck);
router.post('/:id/assign', assignTruck);
router.put('/:id', updateTruck);
router.delete('/:id', deleteTruck)

module.exports = {
  trucksRouter: router
}