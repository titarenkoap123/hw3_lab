const {Truck} = require('../models/Trucks.js');

const sprinter = {
  width: 300,
  length: 250,
  height: 170,
  payload: 1700
};
const smallStraight = {
  width: 500,
  length: 250,
  height: 170,
  payload: 2500
};
const largeStraight = {
  width: 700,
  length: 350,
  height: 200,
  payload: 4000
};

const saveTruck = async ({userId, type}) => {
  const truck = new Truck({
    created_by: userId,
    type
  });
  switch(type) {
    case ('SPRINTER'):
      truck.dimensions = sprinter;
      break;
    case ('SMALL STRAIGHT'):
      truck.dimensions = smallStraight;
      break;
    case ('LARGE STRAIGHT'):
      truck.dimensions = largeStraight;
      break;
  }

  return truck.save();
}

const findTrucks = async (driverId) => {
  return Truck.find({"created_by": driverId});
}

const findTruck = async (truckId) => {
  return Truck.findById(truckId);
}

const findAndAssignTruck = async ({truckId, driverId}) => {
  const truckArray = await Truck.find();
  const checkTrucks = truckArray.filter(truck => {
    return truck.assigned_to === driverId;
  })
  const truck = await Truck.findById(truckId);
  if (truck && !truck.assigned_to && checkTrucks.length === 0) {
    truck.assigned_to = driverId;
    return truck.save();
  }
}

const findAndUpdateTruck = async ({truckId, update}) => {
  const truck = await Truck.findById(truckId);
  if(!truck.assigned_to) {
    truck.type = update;
    switch(update) {
      case ('SPRINTER'):
        truck.dimensions = sprinter;
        break;
      case ('SMALL STRAIGHT'):
        truck.dimensions = smallStraight;
        break;
      case ('LARGE STRAIGHT'):
        truck.dimensions = largeStraight;
        break;
    }
    return truck.save();
  }
}

const findUserAndDeleteTruck = async (truckId) => {
  const truck = await Truck.findById(truckId);
  if(!truck.assigned_to) {
    truck.deleteOne();
  }
}

module.exports = {
  saveTruck,
  findTrucks,
  findTruck,
  findAndAssignTruck,
  findAndUpdateTruck,
  findUserAndDeleteTruck
}